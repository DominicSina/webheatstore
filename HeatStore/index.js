var config = require('./config');
const express = require('express');
const app = express();
const redis=require('redis');

var options = {
  return_buffers: false,
  auth_pass: config.REDIS_PW
  //auth_pass: 'superSecretPassword'
};

app.use(require('express-redis')(6379, '127.0.0.1', options, 'redis'));

var initTemp = function(startDate,endDate,buildingId,res,db){
	//adds one hour until time is reached
	buildingId=0	

	for (var roomId=0; roomId <= 8; roomId = roomId + 1) {
		var previousTemp=25;
	
		for (var i = new Date(startDate.getTime()); i <= endDate; i.setTime(i.getTime() + (60*60*1000))) {
			var tempChange = Math.floor( Math.random() * (1-(-1)+1) + (-1) )
			var newTemp = previousTemp + tempChange
			newTemp = newTemp<15 ? 15 : newTemp
			newTemp = newTemp>25 ? 25 : newTemp
			db.HMSET("buildingId:"+buildingId+"room:Room"+roomId.toString()+"dateTime:"+i.getTime(),"temp", newTemp.toString())
			
			//req.redis.ZADD("buildingId:"+buildingId+"roomId:"+roomId,"buildingId:"+buildingId+"roomId:"+roomId+"date:"+i.toString())
			previousTemp=newTemp
		}
	}
	console.log('ended init')
	res.send('init finished')
}

app.get('/init', function (req, res, next) {
	//year,month,day,hour,minute,second,millisecond
	console.log('started init')

	//not sure why i have to set month to 1 to get february
	var startDate=new Date(1993,1,24,0,0,0,0)
	//var endDate=new Date(1993,2,25,2,0,0,0)
	var endDate=new Date(1993,1,25,0,0,0,0)
	
	initTemp(startDate,endDate,"1",res,req.redis)
});

app.get('/clear', function (req, res, next) {
	console.log('started clear')
	req.redis.flushall(function (err, reply) {
    		if (err) {
      			return res.status(500).end();
    		}
		console.log('ended clear')
		res.send('cleared');
	});
});

app.get('/keys', function (req, res, next) {
	console.log('started get keys')
	//req.redis.keys('*',function (err, reply) {
    	//	if (err) {
      	//		return res.status(500).end();
    	//	}
	//	console.log('ended get keys')
	//	res.send(reply);
	//});
	req.redis.keys('*',redis.print);
});

app.listen(4000, () => console.log('Example app listening on port 4000!'));
